package net.epitech.java.td.repositories.custom;

import java.util.List;

import net.epitech.java.td.domain.Teacher;

public interface TeacherRepositoryCustom {
	
	public List<Teacher> getUnemployedTeachers();

}
