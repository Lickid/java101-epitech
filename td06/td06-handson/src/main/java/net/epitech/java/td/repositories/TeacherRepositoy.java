package net.epitech.java.td.repositories;

import java.util.List;

import net.epitech.java.td.domain.Teacher;
import net.epitech.java.td.repositories.custom.TeacherRepositoryCustom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface TeacherRepositoy extends JpaRepository<Teacher, Integer>,
		QueryDslPredicateExecutor<Teacher> {

	public Teacher findByName(String name);

	public List<Teacher> findByNameAndMail(String name, String mail);

	

}
