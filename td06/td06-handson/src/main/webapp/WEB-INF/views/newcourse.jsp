<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:url value="/" var="webappRoot" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${webappRoot}/resources/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="${webappRoot}/resources/bootstrap-theme.min.css" rel="stylesheet"
	type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Nouveau Cours</title>
</head>
<body>
	<form:form method="post" action="${webappRoot}/course" commandName="course">
		<div class="form-group">

			<form:label class="form-control" path="name">name</form:label>
			<form:input path="name" />
			<form:errors path="name" cssClass="error" />
			<br />
			<form:label class="form-control" path="dayInweek">dayInweek</form:label>
			<form:input path="dayInweek" />
			<form:errors path="dayInweek" cssClass="error" />
			<br />
			<form:label class="form-control" path="dateStart">dateStart</form:label>
			<form:input path="dateStart" />
			<form:errors path="dateStart" cssClass="error" />
			<br />
			<form:label class="form-control" path="duration">duration</form:label>
			<form:input path="duration" />
			<form:errors path="duration" cssClass="error" />
			
			
			<br /> <input type="submit" class="btn btn-default"
				value="Add Cours" />
		</div>
	</form:form>
</body>
</html>