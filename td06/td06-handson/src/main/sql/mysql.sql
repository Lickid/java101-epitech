CREATE  TABLE `epitech`.`Course` (
  `id` INT NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(45) NOT NULL ,
  `dayInWeek` INT NOT NULL ,
  `duration` INT NOT NULL ,
  `idTeacher` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) );
